import 'core-js/stable';
import React from 'react';
import {
  Divider,
  Header,
  Loader,
  Grid,
  Message,
} from 'semantic-ui-react';
import InfiniteScroll from 'react-infinite-scroller';
import './styles';
import { hook as useFlickr } from './services/flickr';
import ImageTile from './components/ImageTile';
import SiteHeader from './components/SiteHeader';

const imageServices = [{ key: 'flickr', text: 'Flickr', value: 'flickr' }];

function App() {
  const {
    error,
    isLoading,
    searchResults,
    images,
    tags,
    setTags,
    addTag,
    handleSearch,
    loadMoreImages,
    hasMoreImages,
    sortableValues,
    handleSort,
    sortValue,
    totalCount,
  } = useFlickr();

  return (
    <div className="app">
      <SiteHeader
        imageServices={imageServices}
        sortableValues={sortableValues}
        sortValue={sortValue}
        handleSearch={handleSearch}
        handleSort={handleSort}
        isLoading={isLoading}
        tags={tags}
        setTags={setTags}
      />
      <Divider horizontal clearing={false} fitted>
        <Header as="h4">
          Results {searchResults.length} / {totalCount > 500 ? '500+' : totalCount}
        </Header>
      </Divider>
      <Divider hidden />
      {error && (
        <Message negative>
          <Message.Header>Could not load images. Please <a href="/">refresh the page to try again</a></Message.Header>
          <p>{error}</p>
        </Message>
      )}
      <InfiniteScroll
        pageStart={1}
        loadMore={loadMoreImages}
        hasMore={!isLoading && hasMoreImages && !error}
      >
        <Grid columns={3} container doubling stackable>
          {searchResults.map((id) => (
            <ImageTile key={id} image={images[id]} component={Grid.Column} addTag={addTag} />
          ))}
          {isLoading && (
            <Grid.Column>
              <Loader active />
            </Grid.Column>
          )}
        </Grid>
      </InfiniteScroll>
    </div>
  );
}

export default App;
