import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import {
  Menu, Dropdown, Icon, Input, Segment, Header,
} from 'semantic-ui-react';
import TagGroup from './TagGroup';

const SiteHeader = ({
  imageServices,
  sortableValues,
  sortValue,
  handleSearch,
  handleSort,
  isLoading,
  tags,
  setTags,
}) => {
  const onChangeSearch = useCallback((_, { value }) => handleSearch(value), [
    handleSearch,
  ]);
  const onChangeSort = useCallback((_, { value }) => handleSort(value), [
    handleSort,
  ]);
  return (
    <header>
      <Menu>
        <Menu.Item>
          <Header as="h1" size="small">
            Image Search
          </Header>
        </Menu.Item>
        <Menu.Item>
          <Dropdown options={imageServices} defaultValue="flickr" simple />
        </Menu.Item>
        <Menu.Item>
          <Icon name="unordered list" />
          <Dropdown
            options={sortableValues}
            defaultValue={sortValue}
            simple
            onChange={onChangeSort}
          />
        </Menu.Item>
        <Menu.Item position="right">
          <Input
            loading={isLoading}
            icon="search"
            placeholder="Search..."
            onChange={onChangeSearch}
          />
        </Menu.Item>
      </Menu>
      <Segment basic>
        <TagGroup tags={tags} setTags={setTags} />
      </Segment>
    </header>
  );
};

SiteHeader.propTypes = {
  imageServices: PropTypes.arrayOf(PropTypes.shape({
    key: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
  })),
  sortableValues: PropTypes.arrayOf(PropTypes.shape({
    key: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
  })),
  sortValue: PropTypes.string.isRequired,
  handleSearch: PropTypes.func.isRequired,
  handleSort: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
  tags: PropTypes.arrayOf(PropTypes.string),
  setTags: PropTypes.func.isRequired,
};

SiteHeader.defaultProps = {
  tags: [],
  imageServices: [],
  sortableValues: [],
};

export default SiteHeader;
