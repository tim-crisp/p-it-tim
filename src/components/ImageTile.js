import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { take } from 'ramda';
import {
  Card, Image, List, Label, Loader,
} from 'semantic-ui-react';

const imageProps = PropTypes.shape({
  id: PropTypes.string,
  url: PropTypes.string,
  title: PropTypes.string,
  hasInfo: PropTypes.bool,
});

const AdditionalInfo = ({ image, addTag }) => (
  <Card.Content>
    <List>
      <List.Item>
        <List.Icon name="user" />
        <List.Content><a target="_blank" href={image.profileLink}>{image.profileName}</a></List.Content>
      </List.Item>
      {image.location && (
        <List.Item>
          <List.Icon name="location arrow" />
          <List.Content>{image.location}</List.Content>
        </List.Item>
      )}
      <List.Item>
        <List.Icon name="calendar" />
        <List.Content>{image.date}</List.Content>
      </List.Item>
      <List.Item>
        <List.Icon name="tags" />
        <List.Content>{take(5, image.tags).map((tag) => (
          <Label as="a" onClick={() => addTag(tag)} className="label--spaced" horizontal key={tag}>{tag}</Label>
        ))}
        </List.Content>
      </List.Item>
    </List>
  </Card.Content>
);

AdditionalInfo.propTypes = {
  image: imageProps.isRequired,
  addTag: PropTypes.func.isRequired,
};

const ImageTile = React.memo(({ component: WrapperComponent, image, addTag }) => (
  <WrapperComponent>
    <Card centered className="full-height">
      <Image src={image.url} wrapped ui={false} as="a" target="_blank" href={image.link} />
      <Card.Content header={image.title} className="card--static" />
      {image.hasInfo ? (
        <AdditionalInfo image={image} addTag={addTag} />
      ) : (
        <Card.Content>
          <Loader active inline="centered" size="mini" />
        </Card.Content>
      )}
    </Card>
  </WrapperComponent>
));

ImageTile.propTypes = {
  image: imageProps.isRequired,
  component: PropTypes.func,
  addTag: PropTypes.func.isRequired,
};

ImageTile.defaultProps = {
  component: Fragment,
};

export default ImageTile;
