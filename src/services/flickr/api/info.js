import axios from 'axios';

export default ({
  imageId,
  // TODO: move API key to state
  apiKey = 'be471220552b5626cc55c62f43bd6f6f',
} = {}) => axios({
  method: 'get',
  params: {
    method: 'flickr.photos.getInfo',
    api_key: apiKey,
    format: 'json',
    nojsoncallback: 1,
    photo_id: imageId,
  },
  url: 'https://www.flickr.com/services/rest/',
  responseType: 'json',
});
