import axios from 'axios';

export default ({
  query,
  tags = [],
  currentPage,
  // TODO: move API key to state
  apiKey = 'be471220552b5626cc55c62f43bd6f6f',
  sortValue,
} = {}) => axios({
  method: 'get',
  params: {
    method: 'flickr.photos.search',
    text: query,
    api_key: apiKey,
    format: 'json',
    nojsoncallback: 1,
    tags: ['safe', ...tags].join(','),
    per_page: 10,
    // per_page: 1,
    page: currentPage,
    sort: sortValue,
  },
  url: 'https://www.flickr.com/services/rest/',
  responseType: 'json',
});
