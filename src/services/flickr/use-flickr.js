import { useReducer, useCallback, useEffect } from 'react';
import reducer from './reducer';
import * as actions from './actions';
import {
  getTags,
  getSearchQuery,
  getSearchResults,
  getIsLoading,
  getErrorMessage,
  getCurrentPage,
  getPageCount,
  getSortValue,
  getTotalCount,
  extractImageInfo,
} from './selectors';
import { search as fetchImageSearch, info as fetchImageInfo, sortableValues } from './api';

const initialState = {
  search: {
    isLoading: true,
    currentPage: 1,
    tags: [],
    sortValue: sortableValues[0].value,
  },
};

const useFlickr = () => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const handleSearch = useCallback((query) => dispatch(actions.flickrSearch({ query })), []);
  const addTag = useCallback((tag) => dispatch(actions.addTag(tag)), []);
  const removeTag = useCallback((tag) => dispatch(actions.removeTag(tag)), []);
  const setTags = useCallback((tags) => dispatch(actions.setTags(tags)), []);
  const loadMoreImages = useCallback(() => dispatch(actions.loadMoreImages()), []);
  const handleSort = useCallback((sortValue) => dispatch(actions.setSort(sortValue)), []);

  const query = getSearchQuery(state);
  const tags = getTags(state);
  const searchResults = getSearchResults(state);
  const isLoading = getIsLoading(state);
  const currentPage = getCurrentPage(state);
  const pageCount = getPageCount(state);
  const sortValue = getSortValue(state);
  const totalCount = getTotalCount(state);

  window.flickr = state;

  const loadImageData = useCallback((imageId) => {
    dispatch(actions.setImageLoading(imageId));
    fetchImageInfo({ imageId })
      .then(({ data }) => {
        dispatch(actions.setImageInfo(imageId, extractImageInfo(data)));
      })
      .catch(() => {
        // TODO: handle error similar to how search handles it
      });
  }, []);

  // trigger search when any attributes change
  useEffect(() => {
    const timeout = setTimeout(() => {
      fetchImageSearch({
        query,
        tags,
        currentPage,
        sortValue,
      })
        .then((result) => {
          dispatch(
            actions.setResults({
              images: result.data.photos.photo,
              clear: result.data.photos.page === 1,
              pageCount: result.data.photos.pages,
              totalCount: result.data.photos.total,
            }),
          );
          setTimeout(() => {
            result.data.photos.photo.map(({ id }) => loadImageData(id));
          }, 100);
        })
        .catch(({ message }) => {
          dispatch(actions.flickrSearchError(message));
        });
    }, 250);
    return () => clearTimeout(timeout);
  }, [query, tags, currentPage, sortValue, loadImageData]);

  // invalidate 20 items from the cache periodically
  useEffect(() => {
    const clearCacheInterval = setInterval(() => {
      dispatch(actions.clearCache(20));
    }, 10000);
    return () => clearInterval(clearCacheInterval);
  }, []);

  return {
    handleSearch,
    removeTag,
    addTag,
    setTags,
    query,
    tags,
    searchResults,
    images: state.images,
    isLoading,
    error: getErrorMessage(state),
    loadMoreImages,
    hasMoreImages: currentPage < pageCount,
    sortableValues,
    handleSort,
    sortValue,
    totalCount,
    loadImageData,
  };
};

export default useFlickr;
