import assert from 'assert';

import {
  flickrSearch,
  FLICKR_SEARCH,
  flickrSearchError,
  FLICKR_SEARCH_ERROR,
  addTag,
  ADD_TAG,
  removeTag,
  REMOVE_TAG,
  setTags,
  SET_TAGS,
  setResults,
  SET_RESULTS,
  clearCache,
  CLEAR_CACHE,
  loadMoreImages,
  FLICKR_LOAD_MORE,
  setSort,
  FLICKR_SET_SORT,
} from './actions';

describe('services/flickr/actions', () => {
  it('#flickrSearch', () => {
    const payload = 'fake payload';

    const actual = flickrSearch(payload);
    const expected = {
      type: FLICKR_SEARCH,
      payload,
    };

    assert.deepEqual(actual, expected, 'should return an object corresponding to the action type');
  });
  it('#flickrSearchError', () => {
    const message = 'fake error message';

    const actual = flickrSearchError(message);
    const expected = {
      type: FLICKR_SEARCH_ERROR,
      message,
    };

    assert.deepEqual(actual, expected, 'should return an object corresponding to the action type');
  });
  it('#addTag', () => {
    const tag = 'fake tag';

    const actual = addTag(tag);
    const expected = {
      type: ADD_TAG,
      tag,
    };

    assert.deepEqual(actual, expected, 'should return an object corresponding to the action type');
  });
  it('#removeTag', () => {
    const tag = 'fake tag';

    const actual = removeTag(tag);
    const expected = {
      type: REMOVE_TAG,
      tag,
    };

    assert.deepEqual(actual, expected, 'should return an object corresponding to the action type');
  });
  it('#setTags', () => {
    const tags = 'fake tags';

    const actual = setTags(tags);
    const expected = {
      type: SET_TAGS,
      tags,
    };

    assert.deepEqual(actual, expected, 'should return an object corresponding to the action type');
  });
  it('#setResults', () => {
    const results = {
      images: 'fake images',
      pageCount: 'fake page count',
      clear: 'fake clear',
      totalCount: 'fake total count',
    };

    const actual = setResults(results);
    const expected = {
      type: SET_RESULTS,
      ...results,
    };

    assert.deepEqual(actual, expected, 'should return an object corresponding to the action type');
  });
  it('#clearCache', () => {
    const count = 'fake count';

    const actual = clearCache(count);
    const expected = {
      type: CLEAR_CACHE,
      count,
    };

    assert.deepEqual(actual, expected, 'should return an object corresponding to the action type');
  });
  it('#loadMoreImages', () => {

    const actual = loadMoreImages();
    const expected = {
      type: FLICKR_LOAD_MORE,
    };

    assert.deepEqual(actual, expected, 'should return an object corresponding to the action type');
  });
  it('#setSort', () => {
    const value = 'fake sort value';

    const actual = setSort(value);
    const expected = {
      type: FLICKR_SET_SORT,
      value,
    };

    assert.deepEqual(actual, expected, 'should return an object corresponding to the action type');
  });
})
