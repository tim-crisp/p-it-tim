import assert from 'assert';

import reducer, { initialSearchPayload } from './reducer';
import {
  FLICKR_SEARCH,
} from './actions';

describe('services/flickr/reducer', () => {
  describe('#FLICKR_SEARCH', () => {
    it('should merge the payload in the event that the current search payload is undefined', () => {
      const initialState = {};
      const payload = {
        fake: 'payload',
      };
      const actual = reducer(initialState, {
        type: FLICKR_SEARCH,
        payload,
      });
      const expected = {
        search: {
          ...payload,
          ...initialSearchPayload,
        },
      };
      assert.deepEqual(actual, expected);
    });
    it('should merge the payload with any existing search payload', () => {
      const searchPayload = {
        fakeSearch: 'payload',
      };
      const initialState = {
        search: {
          ...searchPayload,
        },
      };
      const payload = {
        fake: 'payload',
      };
      const actual = reducer(initialState, {
        type: FLICKR_SEARCH,
        payload,
      });
      const expected = {
        search: {
          ...payload,
          ...initialSearchPayload,
          ...searchPayload,
        },
      };
      assert.deepEqual(actual, expected);
    });
  });
})
