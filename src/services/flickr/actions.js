
export const FLICKR_SEARCH = 'FLICKR_SEARCH';
export const FLICKR_SEARCH_ERROR = 'FLICKR_SEARCH_ERROR';
export const FLICKR_LOAD_MORE = 'FLICKR_LOAD_MORE';
export const FLICKR_IMAGE_LOADING = 'FLICKR_IMAGE_LOADING';
export const FLICKR_SET_IMAGE_INFO = 'FLICKR_SET_IMAGE_INFO';
export const SET_RESULTS = 'SET_RESULTS';
export const SET_TAGS = 'SET_TAGS';
export const ADD_TAG = 'ADD_TAG';
export const REMOVE_TAG = 'REMOVE_TAG';
export const CLEAR_CACHE = 'CLEAR_CACHE';
export const FLICKR_SET_SORT = 'FLICKR_SET_SORT';

export const flickrSearch = (payload) => ({
  type: FLICKR_SEARCH,
  payload,
});

export const flickrSearchError = (message) => ({
  type: FLICKR_SEARCH_ERROR,
  message,
});

export const addTag = (tag) => ({
  type: ADD_TAG,
  tag,
});

export const removeTag = (tag) => ({
  type: REMOVE_TAG,
  tag,
});

export const setTags = (tags) => ({
  type: SET_TAGS,
  tags,
});

export const setResults = ({
  images, pageCount, clear, totalCount,
}) => ({
  type: SET_RESULTS,
  images,
  pageCount,
  clear,
  totalCount,
});

export const clearCache = (count) => ({
  type: CLEAR_CACHE,
  count,
});

export const loadMoreImages = () => ({
  type: FLICKR_LOAD_MORE,
});

export const setSort = (value) => ({
  type: FLICKR_SET_SORT,
  value,
});

export const setImageLoading = (imageId) => ({
  type: FLICKR_IMAGE_LOADING,
  imageId,
});

export const setImageInfo = (imageId, payload) => ({
  type: FLICKR_SET_IMAGE_INFO,
  imageId,
  payload,
});
