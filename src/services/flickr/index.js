
// setTimeout to clear some images from the cache that aren't in the search results

export { default as hook } from './use-flickr';
