import rewiremock from 'rewiremock';

rewiremock.overrideEntryPoint(module); // this is important
export { rewiremock };
