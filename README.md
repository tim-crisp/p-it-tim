# Flickr API Client

[demo (http://parkit.crisp.work)](http://parkit.crisp.work)

## Setup

To get this project running locally do the following:

Ensure you have yarn installed globally:
```
npm i -g yarn
```
alternatively replace `yarn` with `npm`

```
yarn start
```

## Architectural Decisions

- Each ***service*** exposes a common interface so the consumer doesn't have to be aware of specific service implementation. This is done through the means of a react hook.
- For the service that has been implemented (flickr), the state is hoisted to the top to make mutations and general operations easier to reason with. This could've been done using redux, but the newer `useReducer` hook provides us with everything we need with a similar API.
- There is a dropdown on the UI to allow the user to select a service provider. For the purposes of this task flickr is the only service, but due to how the application is architected, the process of implementing other services would be relatively quick.
- The search results and the actual data itself are split into two individual components. This is to prevent stale data existing in one of either of the two sources. Having one source of truth for the data that encapsulates an image is also more resilient and easier to reason with, especially when dealing with an immutable data structure.
- We memoize (using `React.memo`) certain components to ensure that the reconciliation algorithm is aware of the fact that we don't need to re-render components if a shallow comparison of its props evaluates to true.

### Hook API
As mentioned previously, an object of common attributes is to be exposed by each service, so that the component consuming the service can be written in a way that doesn't tie it to any particular provider.
Below is a rough outline of the API that additional services (when implemented) would follow:
```js
{
  handleSearch: Function(String query),
  removeTag: Function(String tag),
  addTag: Function(String tag),
  setTags: Function(String[] tags),
  query: String,
  tags: [String],
  searchResults: [String],
  images: Object,
  isLoading: Boolean,
  error: String || undefined,
  loadMoreImages: Function(),
  hasMoreImages: Boolean,
  sortableValues: [Object],
  handleSort: Function(String sortType),
  sortValue: String,
  totalCount: Number,
}
```

## Testing

```
yarn test:unit
```

A few unit tests have been implemented to demonstrate a rough idea of what might be tested. I didn't have time to test all the react components/hooks and logic but if I could've invested more time in it I would've used libraries like `sinon` to stub out and/or spy on dependencies/imports to verify the interactions between the test subject and its external dependencies. It might also be necessary in some cases to mock external components so that in the event of an external dependency breaking, the broken dependency doesn't inflict failure on the test subject.

## Lint

eslint is configured to use airbnb's javascript industry standards.

```
yarn lint
```
